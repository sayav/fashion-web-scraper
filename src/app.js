const steelo = require('steelo');

steelo.init({ database: 'mongodb://localhost/steelo-dev' });

(async () => {
  await steelo.homepage({
    url: 'http://www.lyst.com', // Site URL
    menuLink: '[data-category=clothing] a', // Links on homepage menu
    men: `$(el).parents('[data-gender=men]').length`, // Condition if link is for men's clothes
    women: `$(el).parents('[data-gender=women]').length` // Condition if link is for women's clothes
  });

  // await steelo.saveProductLinks({
  //   linkEl: '.product-card__short-description' // Element containing links ('a') on category page
  // });

  // await steelo.saveProducts({
  //   imageUrlEl: '[is=gallery-thumbnails] a', // Element containing image links on category page
  //   brand: `$('[itemprop="brand"]').text().trim()`, // Brand on product page
  //   shortDescription: `$('.product-header [itemprop="name"]').text().trim()`, // Short Description on product page
  //   longDescription: `$('.product-description').find('p').text().trim()`, // Long Description on product page
  //   color: `longDescription.split('Color:')[1].trim()` // Color on product page
  // });
})();
